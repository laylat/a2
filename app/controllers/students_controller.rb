class StudentsController < ApplicationController
  before_action :set_student, only: [:show, :edit, :update, :destroy]

  def index
    @students = Student.all
  end


  def show
  end

  def new
    @student = Student.new
  end

  def edit
  end


  def create
    @student = Student.new(student_params)

    respond_to do |format|
      if @student.save
        redirect_to students_path
      else
        render :new

      end
    end
  end

  def update
    respond_to do |format|
      if @student.update(student_params)
        redirect_to @student
      else
        render :edit 
      end
    end
  end


  def destroy
    @student.destroy
    respond_to do |format|
      format.html { redirect_to students_url, notice: 'Student removed from database.' }
    end
  end

  private

    def set_student
      @student = Student.find(params[:id])
    end


    def student_params
      params.require(:student).permit(:Name, :Gender, :Weight, :Height, :Colour, :GPA, :PrimaryCampus, :Faculty)
    end
end
